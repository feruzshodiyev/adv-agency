import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";
import SecureLS from "secure-ls";

const ls = new SecureLS({ isCompression: false });

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    accessToken: null,
    refreshToken: null,
    mainSidebarCollapsed: true,
    user: {},
    tableSize: "mini"
  },
  mutations: {
    authenticate(state, tokenResponse) {
      let { access_token, refresh_token } = tokenResponse;
      state.accessToken = access_token;
      state.refreshToken = refresh_token;
    },
    userData(state, userData) {
      this.state.user = userData;
    },
    signOut(state) {
      state.accessToken = null;
      state.refreshToken = null;
    },
    toggleMainSidebar(state, mainSidebarCollapsed) {
      state.mainSidebarCollapsed = mainSidebarCollapsed;
    },
    updateTableSize(state, size) {
      this.state.tableSize = size;
    },
    updatePasswordModifiedDate(state, value) {
      state.passwordModifiedDate = value;
    }
  },
  actions: {
    changeTableSize(ctx, size) {
      ctx.commit("updateTableSize", size);
    }
  },
  plugins: [
    createPersistedState({
      key: "uchamiz-ui",
      paths: [
        "accessToken",
        "refreshToken",
        "locale",
        "mainSidebarCollapsed",
        "user",
        "tableSize"
      ],
      storage: {
        getItem: key => ls.get(key),
        setItem: (key, value) => ls.set(key, value),
        removeItem: key => ls.remove(key)
      }
    })
  ]
});
