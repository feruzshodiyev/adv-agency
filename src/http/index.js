import axios from "axios";
import store from "../store";
import router from "../router";

let instance = axios.create({
  baseURL: process.env.VUE_APP_BASE_API_URL,
  transformRequest: (data, headers) => {
    headers["Authorization"] = `Bearer ${store.state.accessToken}`;
    headers["Content-Type"] = "application/json";
    return JSON.stringify(data);
  }
});

instance.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    if (error.response.status !== 401) {
      return Promise.reject(error);
    }
    if (!store.state.refreshToken) {
      store.commit("signOut");
      router.push("/auth");
      return Promise.reject(error);
    }
    return axios
      .post(
        `${process.env.VUE_APP_BASE_API_URL}auth/oauth/token?grant_type=refresh_token&refresh_token=${store.state.refreshToken}`,
        null,
        {
          headers: {
            Authorization: `Basic ${process.env.VUE_APP_BASIC_AUTH_TOKEN}`
          }
        }
      )
      .then(({ data }) => {
        store.commit("authenticate", data);
        return instance(error.response.config);
      })
      .catch(() => {
        store.commit("signOut");
        router.push("/auth");
        return Promise.reject(error);
      });
  }
);

export default instance;
