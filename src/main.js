import Vue from 'vue'
import ElementUI from "element-ui";
import "./assets/themes/core.scss";
import "line-awesome/dist/line-awesome/scss/line-awesome.scss";
import locale from "element-ui/lib/locale/lang/ru-RU";
import App from "./App.vue";
import store from "./store";
import http from "./http";
import LineAwesome from "line-awesome/dist/line-awesome/css/line-awesome.min.css";
import router from "@/router";

import VInputmask from "v-inputmask";
import moment from "moment";

moment.locale(store.state.locale);
Vue.use(VInputmask);
Vue.use(LineAwesome);
Vue.config.productionTip = false;
Vue.use(ElementUI, { locale });
Vue.prototype.$http = http;
Vue.prototype.$moment = moment;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
