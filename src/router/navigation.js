import UserList from "@/views/users/UserList";
import UserForm from "@/views/users/UserForm";
import AdvFormList from "@/views/advertiseForm/AdvFormList";
import AdvForm from "@/views/advertiseForm/AdvForm";
import AdvTypes from "@/views/advertiseType/AdvTypes";
import ContractList from "@/views/contract/ContractList";
import ContractForm from "@/views/contract/ContractForm";
import OrderList from "@/views/orders/OrderList";
import OutSourcing from "@/views/outsourcing/OutSourcing";

const mainNav = [
  //.......user.....//
  {
    main: true,
    isSubmenu: false,
    title: "Foydalanuvchilar",
    icon: "las la-lg la-users",
    name: "UserList",
    path: "user/list",
    component: UserList,
    meta: { authorize: [] }
  },
  {
    main: false,
    name: "UserAdd",
    path: "user/add",
    component: UserForm,
    meta: { authorize: [] }
  },
  {
    main: false,
    name: "UserEdit",
    path: "user/edit/:id",
    component: UserForm,
    meta: { authorize: [] }
  },

    //..........adv-form.......//
  {
    main: true,
    isSubmenu: false,
    title: "Reklama joyash shakllari",
    icon: "las la-lg la-list",
    name: "AdvFormList",
    path: "adv-form/list",
    component: AdvFormList,
    meta: { authorize: [] }
  },
  {
    main: false,
    name: "AdvFormAdd",
    path: "adv-form/add",
    component: AdvForm,
    meta: { authorize: [] }
  },
  {
    main: false,
    name: "AdvFormEdit",
    path: "adv-form/edit/:id",
    component: AdvForm,
    meta: { authorize: [] }
  },

  //..........adv-type.......//
  {
    main: true,
    isSubmenu: false,
    title: "Reklama turlari",
    icon: "las la-lg la-list",
    name: "Advtypes",
    path: "adv-type/list",
    component: AdvTypes,
    meta: { authorize: [] }
  },

  //..........contract.......//
  {
    main: true,
    isSubmenu: false,
    title: "Shartnomalar",
    icon: "las la-lg la-file-alt",
    name: "ContractList",
    path: "contract/list",
    component: ContractList,
    meta: { authorize: [] }
  },
  {
    main: false,
    name: "ContractAdd",
    path: "contract/add",
    component: ContractForm,
    meta: { authorize: [] }
  },
  {
    main: false,
    name: "ContractEdit",
    path: "contract/edit/:id",
    component: ContractForm,
    meta: { authorize: [] }
  },

  //..........orders.......//
  {
    main: true,
    isSubmenu: false,
    title: "Buyurtmalar",
    icon: "las la-lg la-file-alt",
    name: "OrderList",
    path: "order/list",
    component: OrderList,
    meta: { authorize: [] }
  },

  //..........outsourcing.......//
  {
    main: true,
    isSubmenu: false,
    title: "Autsorsing",
    icon: "las la-lg la-file-alt",
    name: "OutSourcing",
    path: "outsourcing/list",
    component: OutSourcing,
    meta: { authorize: [] }
  },

];

export default mainNav;
